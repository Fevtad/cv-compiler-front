import { Output } from '@angular/core';
import { Component, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'photo-tip-one',
  templateUrl: './photo-tip-one.component.html',
  styleUrls: ['./photo-tip-one.component.scss']
})
export class PhotoTipOneComponent implements OnInit {

  @Output() next: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  nextPhoto(){
    this.next.emit(true);
  }

}

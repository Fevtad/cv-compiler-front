import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { SectionFormat, UserCV } from '../../admin-page/objects/user-cv.object';
import { CvService } from '../services/cv.service';

@Component({
  selector: 'cv-detail',
  templateUrl: './cv-detail.component.html',
  styleUrls: ['./cv-detail.component.scss']
})
export class CvDetailComponent implements OnInit {

  viewType = {
    uploaded: true,
    recommended: true,
    edited: false,
  };

  cvId: string;
  userCvList: UserCV[] = [];
  sections: SectionFormat[] = [];
  successMessage = null;
  errorMessage = null;

  educationSection = new SectionFormat();
  workSection = new SectionFormat();
  skillSection = new SectionFormat();
  personalSection = new SectionFormat();
  acheivementSection = new SectionFormat();
  referenceSection = new SectionFormat();

  educationVersion = new SectionFormat();
  workVersion = new SectionFormat();
  skillVersion = new SectionFormat();
  personalVersion = new SectionFormat();
  acheivementVersion = new SectionFormat();
  referenceVersion = new SectionFormat();

  versionSection = [];

  constructor(private router: ActivatedRoute, private cvService: CvService, private authService: CustomeAuthService) { }

  ngOnInit() {
    this.updateCvDetailComponent();
    this.cvService.userCVListEmitter.subscribe(
      data => {
        this.userCvList = data.sections;
        for (const section of this.userCvList) {
          if (section.section.name === 'education') {
            this.educationSection = section;
          } else if (section.section.name === 'skills') {
            this.skillSection = section;
          } else if (section.section.name === 'personalInfo') {
            this.personalSection = section;
          } else if (section.section.name === 'work') {
            this.workSection = section;
          } else if (section.section.name === 'achievement') {
            this.acheivementSection = section;
          } else if (section.section.name === 'reference') {
            this.referenceSection = section;
          }
        }
      }
    );
  }

  updateCvDetailComponent(){
    this.router.params.subscribe(
      param => {
        // this.cvService.getUserCvDetail(param.id);
        this.cvId = param.id;
      }
    );
    this.cvService.getUserCvDetail(this.authService.getId());
    setTimeout(() => {
      this.errorMessage = null;
      this.successMessage = null;
    }, 5000);
  }
  readyToReplace(){
    if (this.educationSection.recommended[0] && this.workSection.recommended[0] && this.skillSection.recommended && 
      this.personalSection.recommended.first_name && this.acheivementSection.recommended ){
        return true;
      }
  }
  replaceAll() {
    this.educationSection.description = this.educationSection.recommended;
    this.workSection.description = this.workSection.recommended;
    this.skillSection.description = this.skillSection.recommended;
    this.personalSection.description = this.personalSection.recommended;
    this.acheivementSection.description = this.acheivementSection.recommended;
    this.referenceSection.description = this.referenceSection.recommended;

    this.sections.push(this.educationSection);
    this.sections.push(this.workSection);
    this.sections.push(this.skillSection);
    this.sections.push(this.personalSection);
    this.sections.push(this.acheivementSection);
    this.sections.push(this.referenceSection);
    this.onUpdateAll();
  }
  saveVersion() {
    this.versionSection.push(this.educationVersion);
    this.versionSection.push(this.personalVersion);
    this.versionSection.push(this.referenceVersion);
    this.versionSection.push(this.skillVersion);
    this.versionSection.push(this.workVersion);
    this.versionSection.push(this.acheivementVersion);
    this.cvService.addCvHistory(this.cvId, this.versionSection);
    this.cvService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateCvDetailComponent();
    });

    this.cvService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error;
      }
    );
  }
  onUpdateAll() {
    this.cvService.updateCv(this.cvId, this.sections);
    this.cvService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateCvDetailComponent();
    });

    this.cvService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error;
      }
    );
  }

  onUpdateCv(userForm: SectionFormat) {
    this.sections.push(userForm);
    console.log(this.sections);
    this.cvService.updateCv(this.cvId, this.sections);
    this.cvService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateCvDetailComponent();
    });

    this.cvService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error;
      }
    );
  }
  saveEducationVersion(educationSection: any) {
    this.educationSection.description = educationSection.uploaded;
    this.educationVersion = educationSection;
    // this.versionSection.push(educationSection);
  }
  saveWorkVersion(workSection: any) {
    this.workSection.description = workSection.uploaded;
    this.workVersion = workSection;
    // this.versionSection.push(workSection);
  }
  saveAcheivementVersion(acheivementSection: any) {
    this.acheivementSection.description = acheivementSection.uploaded;
    this.acheivementVersion = acheivementSection;
    // this.versionSection.push(acheivementSection);
  }
  saveSkillVersion(skillSection: any) {
    this.skillSection.description = skillSection.uploaded;
    this.skillVersion = skillSection;
    // this.versionSection.push(skillSection);
  }
  savePersonalVersion(personalSection: any) {
    this.personalSection.description = personalSection.uploaded;
    this.personalVersion = personalSection;
    // this.versionSection.push(personalSection);
  }
  saveReferenceVersion(referenceSection: any) {
    this.referenceSection.description = referenceSection.uploaded;
    this.referenceVersion = referenceSection;
    // this.versionSection.push(referenceSection);
  }
  updateRecommendation(event){
  }
}

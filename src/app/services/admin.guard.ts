import { Injectable } from '@angular/core';
import { CanActivate,Router } from '@angular/router';
import { CustomeAuthService } from '../custome-auth/custome-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private authService: CustomeAuthService, private router: Router) {
  }

  canActivate() {
        if (this.authService.isAdmin()) {
          return true;
        }else {
          this.router.navigate(['pages/unauthorized']);
          return false;
        }
  }
  
}

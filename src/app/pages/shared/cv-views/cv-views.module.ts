import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EducationViewComponent } from './education-view/education-view.component';
import { WorkViewComponent } from './work-view/work-view.component';
import { SkillViewComponent } from './skill-view/skill-view.component';
import { PersonalViewComponent } from './personal-view/personal-view.component';
import { AcheivementViewComponent } from './acheivement-view/acheivement-view.component';
import { EducationTableComponent } from './education-view/education-table/education-table.component';
import { NbAlertModule, NbButtonModule, NbCardModule, NbIconModule, NbInputModule, NbListModule, NbSelectModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { WorkTableComponent } from './work-view/work-table/work-table.component';
import { PersonalTableComponent } from './personal-view/personal-table/personal-table.component';
import { SkillTableComponent } from './skill-view/skill-table/skill-table.component';
import { AcheivementTableComponent } from './acheivement-view/acheivement-table/acheivement-table.component';
import { ReferenceViewComponent } from './reference-view/reference-view.component';
import { ReferenceTableComponent } from './reference-view/reference-table/reference-table.component';



@NgModule({
  declarations: [EducationViewComponent, WorkViewComponent, SkillViewComponent, PersonalViewComponent, AcheivementViewComponent, EducationTableComponent, WorkTableComponent, PersonalTableComponent, SkillTableComponent, AcheivementTableComponent, ReferenceViewComponent, ReferenceTableComponent],
  imports: [
    CommonModule,
    NbCardModule,
    FormsModule,
    NbInputModule,
    NbAlertModule,
    NbButtonModule,
    NbListModule,
    NbIconModule,
    NbSelectModule,
  ],
  exports: [EducationViewComponent, WorkViewComponent, SkillViewComponent, PersonalViewComponent, AcheivementViewComponent, ReferenceViewComponent],
})
export class CvViewsModule { }

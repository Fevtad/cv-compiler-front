import { Component, OnInit, TemplateRef } from '@angular/core';
import Swiper from 'swiper/bundle';
import { NbDialogService } from '@nebular/theme';
import { UploadCvService } from '../upload-cv.service';

@Component({
  selector: 'choose-template',
  templateUrl: './choose-template.component.html',
  styleUrls: ['./choose-template.component.css']
})
export class ChooseTemplateComponent implements OnInit {
  mySwiper: any;
  hasUploaded = false;

  constructor(private dialogService: NbDialogService, private uploadCvService: UploadCvService) { }

  ngOnInit() {
    this.uploadCvService.hasUploaded();
    this.uploadCvService.hasUploadedEmitter.subscribe(
      data => {
        this.hasUploaded = data;
      }
    );
    this.mySwiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      initialSlide: 2,
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });
  }
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      { context: 'The photo section will be left blank!' });
  }

}

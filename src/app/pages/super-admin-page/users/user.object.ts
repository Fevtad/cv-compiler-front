export class UserPaginator {
    public current_page: number;
    public first_page_url: string;
    public from: number;
    public last_page: number;
    public last_page_url: string;
    public next_page_url: string;
    public path: string;
    public per_page: number;
    public prev_page_url: string;
    public to: number;
    public total: number;
    public data: User[];

    constructor() {
        this.current_page = null;
        this.from = null;
        this.last_page = null;
        this.per_page = null;
        this.to = null;
        this.total = null;
        this.first_page_url = '';
        this.last_page_url = '';
        this.next_page_url = '';
        this.prev_page_url = '';
        this.path = '';

    }
}
export class User {
    public _id: string;
    public email: string;
    public firstName: string;
    public lastName: string;
    public country: string;
    public phoneNumber: string;
    public role: string;
    public password: string;
    constructor() {
        this._id = null;
        this.email = null;
        this.firstName = null;
        this.lastName = null;
        this.country = null;
        this.phoneNumber = null;
        this.role = null;
        this.password = null;
    }
}

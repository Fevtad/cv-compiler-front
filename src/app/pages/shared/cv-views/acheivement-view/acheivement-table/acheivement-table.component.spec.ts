import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcheivementTableComponent } from './acheivement-table.component';

describe('AcheivementTableComponent', () => {
  let component: AcheivementTableComponent;
  let fixture: ComponentFixture<AcheivementTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcheivementTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcheivementTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

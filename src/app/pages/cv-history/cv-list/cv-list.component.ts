import { Component, OnInit, TemplateRef } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { CvService } from '../services/cv.service';

@Component({
  selector: 'ngx-cv-list',
  templateUrl: './cv-list.component.html',
  styleUrls: ['./cv-list.component.css']
})
export class CvListComponent implements OnInit {
  // cvList = [
  //   {
  //     id: 1,
  //     name: 'Document1',
  //     date: '2018/09/09',
  //   },
  //   {
  //     id: 2,
  //     name: 'Document2',
  //     date: '2018/39/19',
  //   },
  // ];
  cvList = [];
  latestCv = [];
  selectedHistory = null;
  successMessage = null;
  errorMessage = null;

  constructor(private cvService: CvService, private authService: CustomeAuthService,
    private dialogService: NbDialogService) { }

  ngOnInit() {
    this.updateCvListComponent();
    this.cvService.historyListEmitter.subscribe(
      data => {
        this.cvList = data.historys;
        this.latestCv = data.latestCv;
      }
    );
  }
  updateCvListComponent() {
    this.cvService.getAllHistory(this.authService.getId());
    setTimeout(() => {
      this.errorMessage = null;
      this.successMessage = null;
    }, 5000);
  }
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      { context: 'You\'re done!' });
  }
  selectHistory(history: any){
    this.selectedHistory = history;
  }
  deleteHistory() {
    this.cvService.deleteCvHistory(this.selectedHistory._id);
    this.cvService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateCvListComponent();
    });
    this.cvService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error;
      },
    );
  }

}

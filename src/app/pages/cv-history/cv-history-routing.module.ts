import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CvDetailComponent } from './cv-detail/cv-detail.component';
import { CvHistoryComponent } from './cv-history.component';
import { CvListComponent } from './cv-list/cv-list.component';

const routes: Routes = [
  {
    path: '',
    component: CvHistoryComponent,
    children: [
      {
        path: 'cv-list',
        component: CvListComponent,
      },
      {
        path: 'cv-detail/:id',
        component: CvDetailComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CvHistoryRoutingModule { }

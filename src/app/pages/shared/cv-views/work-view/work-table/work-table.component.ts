import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../../custome-auth/custome-auth.service';
import { Work } from '../../../cv-forms/work/work.object';

@Component({
  selector: 'work-table',
  templateUrl: './work-table.component.html',
  styleUrls: ['./work-table.component.scss']
})
export class WorkTableComponent implements OnInit {

  @Input() workList: Work[];
  @Input() type: string;
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
  }
  cancel(){
    this.onCancel.emit(true);
  }
  save(){
    this.onSave.emit(this.workList);
  }
  isUser(){
    return this.authService.isUser();
  }

}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CvFormService } from '../services/cv-form.service';
import { Education } from './education.object';

@Component({
  selector: 'education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  @Output() onAddEducation: EventEmitter<any> = new EventEmitter();
  education = new Education();
  educationList: Education[] = [];
  isDone = false;

  constructor(private cvFormService: CvFormService) { }

  ngOnInit() {
    this.educationList = this.cvFormService.getEducation();
    if (this.educationList[0]){
      for (const education of this.educationList){
        this.education = education;
        console.log(education);
      }
    } else {
      this.education = new Education();
    }
  }

  submitEducation(){
    this.educationList.push(this.education);
  }
  done(){
    this.cvFormService.saveEducation(this.educationList);
    this.onAddEducation.emit(this.educationList);
    this.isDone = true;
  }

  resetEducation(){
    this.education = new Education();
  }

}

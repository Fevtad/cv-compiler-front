import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../user.object';

@Component({
  selector: 'ngx-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  @Output() onNewUserAdded: EventEmitter<any> = new EventEmitter();
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  public newUser = new User();
  public phoneValid = true;
  public focus = false;

  constructor() {
  }

  ngOnInit() {
  }

  public hasError(event) {
    this.phoneValid = event;
  }

  public cancel() {
    this.onClose.emit(true);
  }

  public addNewUser() {
    this.onNewUserAdded.emit(this.newUser);
    this.newUser = new User();
    this.onClose.emit(true);
  }

}

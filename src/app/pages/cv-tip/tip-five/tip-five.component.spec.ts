import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipFiveComponent } from './tip-five.component';

describe('TipFiveComponent', () => {
  let component: TipFiveComponent;
  let fixture: ComponentFixture<TipFiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipFiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

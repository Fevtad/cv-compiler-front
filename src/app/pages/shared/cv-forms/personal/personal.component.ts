import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CvFormService } from '../services/cv-form.service';
import { Link, Personal } from './personal.object';

@Component({
  selector: 'personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {

  @Output() onAddPersonal: EventEmitter<any> = new EventEmitter();
  personal: Personal;
  linkArr = [];

  constructor(private cvFormService: CvFormService) { }

  ngOnInit() {
    this.personal = this.cvFormService.getPersonal();
    if (!this.personal.links[0]) {
      this.personal.links = [
        { 'type': '', 'link': '' },
        { 'type': '', 'link': '' },
        { 'type': '', 'link': '' }
      ];
    }

  }

  submitPersonal() {
    let i = this.personal.links.length;
    while (i--) {
      if (this.personal.links[i].type === '' || this.personal.links[i].link === '') {
        this.personal.links.splice(i, 1);
      }
    }
    this.cvFormService.savePersonal(this.personal);
    this.onAddPersonal.emit(this.personal);
  }

  addLink() {
    if (this.linkArr.length < 3) {
      this.linkArr.length += 1;
    }
  }
}

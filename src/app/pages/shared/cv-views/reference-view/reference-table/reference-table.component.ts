import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../../custome-auth/custome-auth.service';
import { Reference } from '../../../cv-forms/reference/reference.object';

@Component({
  selector: 'reference-table',
  templateUrl: './reference-table.component.html',
  styleUrls: ['./reference-table.component.scss']
})
export class ReferenceTableComponent implements OnInit {

  @Input() referenceList: Reference[];
  @Input() type: string;
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
  }
  cancel(){
    this.onCancel.emit(true);
  }
  save(){
    this.onSave.emit(this.referenceList);
  }
  isUser(){
    return this.authService.isUser();
  }

}

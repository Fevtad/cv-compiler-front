import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipThreeComponent } from './tip-three.component';

describe('TipThreeComponent', () => {
  let component: TipThreeComponent;
  let fixture: ComponentFixture<TipThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

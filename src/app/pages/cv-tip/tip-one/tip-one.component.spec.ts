import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipOneComponent } from './tip-one.component';

describe('TipOneComponent', () => {
  let component: TipOneComponent;
  let fixture: ComponentFixture<TipOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

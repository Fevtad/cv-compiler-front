import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../custome-auth/custome-auth.service';
import { Personal } from '../../cv-forms/personal/personal.object';

@Component({
  selector: 'personal-view',
  templateUrl: './personal-view.component.html',
  styleUrls: ['./personal-view.component.scss']
})
export class PersonalViewComponent implements OnInit {

  @Input() personalSection;
  @Input() cvId: string;
  @Output() savePersonalVersion: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateRecommendation: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateCv: EventEmitter<any> = new EventEmitter();
  selectedPersonal = new Personal();
  successMessage = null;
  errorMessage = [];
  type = 'view';

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
    this.savePersonalVersion.emit(this.personalSection);
  }
  selectList(list: Personal) {
    this.selectedPersonal = list;
    this.type = 'edit';
  }

  // when save is clicked
  savePersonal(personal: Personal) {
    this.personalSection.description = personal;
    if (this.isUser()) {
      // update cv for user
      this.onUpdateCv.emit(this.personalSection);
      // emit to the parent for version saving
      this.savePersonalVersion.emit(this.personalSection);
    } else if (this.isAdmin()){
       this.onUpdateRecommendation.emit(this.personalSection);
    }
    this.type = 'view';
  }
  isArray(value){
    return Array.isArray(value);
  }
  keep(){
    this.savePersonal(this.personalSection.uploaded);
  }
  replace(){
    this.savePersonal(this.personalSection.recommended);
  }
  isUser() {
    return this.authService.isUser();
  }
  isAdmin() {
    return this.authService.isAdmin();
  }

}

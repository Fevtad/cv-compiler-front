import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../custome-auth/custome-auth.service';
import { Work } from '../../cv-forms/work/work.object';

@Component({
  selector: 'work-view',
  templateUrl: './work-view.component.html',
  styleUrls: ['./work-view.component.scss']
})
export class WorkViewComponent implements OnInit {

  @Input() workSection;
  @Input() cvId: string;
  @Output() saveWorkVersion: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateRecommendation: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateCv: EventEmitter<any> = new EventEmitter();
  selectedWorkList: Work[] = [];
  successMessage = null;
  errorMessage = [];
  type = 'view';

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
    this.saveWorkVersion.emit(this.workSection);
  }
  selectList(list: Work[]) {
    this.selectedWorkList = list;
    this.type = 'edit';
  }

  // when save is clicked
  saveWorkList(WorkList: Work[]) {
    this.workSection.description = WorkList;
    if (this.isUser()) {
      // update cv for user
      this.onUpdateCv.emit(this.workSection);
      // emit to the parent for version saving
      this.saveWorkVersion.emit(this.workSection);
    } else if (this.isAdmin()){
       this.onUpdateRecommendation.emit(this.workSection);
    }
    this.type = 'view';
  }
  keep(){
    this.saveWorkList(this.workSection.uploaded);
  }
  replace(){
    this.saveWorkList(this.workSection.recommended);
  }
  isUser() {
    return this.authService.isUser();
  }
  isAdmin() {
    return this.authService.isAdmin();
  }

}

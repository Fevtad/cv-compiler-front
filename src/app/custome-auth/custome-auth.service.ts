import { Injectable, EventEmitter } from '@angular/core';
import { NewUser } from './new-user.object';
import { HttpRequestService } from '../services/http-request.service';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})
export class CustomeAuthService {

  token = this.getUserToken();
  header = new HttpHeaders({ 'Content-Type': 'application/json' });
  authErrorEmitter = new EventEmitter();
  authSuccessEmitter = new EventEmitter();
  authenticatedUserEmitter = new EventEmitter();

  constructor(private httprequest: HttpRequestService) { }

  register(user: NewUser) {
    return this.httprequest.sendCustomePostRequest('v1/auth/signup', user)
      .subscribe(
        data => {
          console.log(data);
          this.authSuccessEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.authErrorEmitter.emit(error.error.errors[0].msg);
        }
      );
  }
  authenticate(user: any) {
    return this.httprequest.sendCustomePostRequest('v1/auth/login', user)
      .subscribe(
        data => {
          this.processAuthenticateUser(data);
        },
        error => {
          console.log(error);
          this.authErrorEmitter.emit(error.error.msg);
        }
      );
  }
  processAuthenticateUser(data: any) {
    if (data.token) {
      this.setUserToken(data.token);
      this.setFirstName(data.user.firstName);
      this.setId(data.user._id);
      this.setLastName(data.user.lastName);
      this.setEmail(data.user.email);
      this.setRole(data.user.role);
      this.authSuccessEmitter.emit(data);
    }
  }
  verify(token: any) {
    return this.httprequest.sendCustomePostRequest('v1/auth/verify', {token})
      .subscribe(
        data => {
          console.log(data);
          this.authSuccessEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.authErrorEmitter.emit(error);
        }
      );
  }
  requestPassword(email: any) {
    return this.httprequest.sendCustomePostRequest('v1/auth/forgotpassword', {email})
      .subscribe(
        data => {
          console.log(data);
          this.authSuccessEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.authErrorEmitter.emit(error.error.msg);
        }
      );
  }
  resetPassword(token: any, password: any) {
    return this.httprequest.sendCustomePostRequest('v1/auth/resetpassword', {token, password})
      .subscribe(
        data => {
          console.log(data);
          this.authSuccessEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.authErrorEmitter.emit(error);
        }
      );
  }
  logOut() {
    this.setUserToken('');
    this.setEmail('');
    this.setFirstName('');
    this.setId('');
    this.setLastName('');
    this.setRole('');
  }
  isAuthenticated() {
    return this.getUserToken() !== '';
  }
  setUserToken(userToken: string) {
    localStorage.setItem('tokenAuthKey', userToken);
  }

  setFirstName(firstName: string) {
    localStorage.setItem('firstName', firstName);
  }
  setEmail(email: string) {
    localStorage.setItem('email', email);
  }
  setId(id: string) {
    localStorage.setItem('id', id);
  }
  setLastName(lastName: string) {
    localStorage.setItem('lastName', lastName);
  }

  setRole(role: string) {
    localStorage.setItem('role', role);
  }

  getUserToken() {
    return localStorage.getItem('tokenAuthKey');
  }
  getEmail() {
    return localStorage.getItem('email');
  }
  getId() {
    return localStorage.getItem('id');
  }
  getFirstName() {
    return localStorage.getItem('firstName');
  }
  getLastName() {
    return localStorage.getItem('lastName');
  }
  getRole() {
    return localStorage.getItem('role');
  }
  isSuperAdmin() {
    return (this.getRole() === 'superAdmin');
  }
  isAdmin() {
    return (this.getRole() === 'admin');
  }
  isUser() {
    return (this.getRole() === 'user');
  }
}

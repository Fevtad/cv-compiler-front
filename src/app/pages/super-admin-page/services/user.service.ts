import { Injectable, EventEmitter } from '@angular/core';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { HttpRequestService } from '../../../services/http-request.service';
import { User, UserPaginator } from '../users/user.object';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  token = this.authSerice.getUserToken();
  paginate_count = this.httprequest.paginate_count;
  paginatedUsersEmitter = new EventEmitter<UserPaginator>();
  userListEmitter = new EventEmitter<User>();
  responseEmitter = new EventEmitter();
  errorEmitter = new EventEmitter();

  constructor(private httprequest: HttpRequestService, private authSerice: CustomeAuthService) { }

  addUser(user: User) {
    this.httprequest.sendPostRequest('v1/superadmin/admin/register', user, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          const messages = Object.values(error.error.error);
          this.errorEmitter.emit(messages);
        }
      );
  }
  updateUser(id: string, user: User) {
    this.httprequest.sendPatchRequest('v1/user/update/' + id, user, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          const messages = Object.values(error.error.error);
          this.errorEmitter.emit(messages);
        }
      );
  }
  deleteUser(id: string) {
    this.httprequest.sendDeleteRequest('v1/superadmin/admin/' + id, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          const messages = Object.values(error.error.error);
          this.errorEmitter.emit(messages);
        }
      );
  }
  getAllPaginatedUsers() {
    this.httprequest.sendGetRequest('v1/superadmin/admins?page=1&limit=5', this.token).subscribe(
        data => {
          this.processGetPagiantedUsers(data);
        },
        error => {
          console.log('Something Went Wrong!!!', error);
        },
    );
  }
  getNextPrevUsers(full_url) {
    console.log(full_url);
    this.httprequest.sendCustomGetRequest(full_url + '&paginate_count=' + this.paginate_count, this.token)
        .subscribe(data => {
          this.processGetPagiantedUsers(data);
        });
  }
  processGetPagiantedUsers(data: any) {
    if (data.docs) {
      this.userListEmitter.emit(data.docs);
    }
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../custome-auth/custome-auth.service';

@Component({
  selector: 'acheivement-view',
  templateUrl: './acheivement-view.component.html',
  styleUrls: ['./acheivement-view.component.scss']
})
export class AcheivementViewComponent implements OnInit {

  @Input() acheivementSection;
  @Input() cvId: string;
  @Output() saveAcheivementVersion: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateRecommendation: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateCv: EventEmitter<any> = new EventEmitter();
  selectedAcheivementList = [];
  successMessage = null;
  errorMessage = [];
  type = 'view';

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
    this.saveAcheivementVersion.emit(this.acheivementSection);
  }
  selectList(list: any) {
    this.selectedAcheivementList = list;
    this.type = 'edit';
  }

  // when save is clicked
  saveAcheivementList(acheivementList: any) {
    this.acheivementSection.description = acheivementList;
    if (this.isUser()) {
      // update cv for user
      this.onUpdateCv.emit(this.acheivementSection);
      // emit to the parent for version saving
      this.saveAcheivementVersion.emit(this.acheivementSection);
    } else if (this.isAdmin()){
      console.log(this.acheivementSection)
       this.onUpdateRecommendation.emit(this.acheivementSection);
    }
    this.type = 'view';
  }
  keep(){
    this.saveAcheivementList(this.acheivementSection.uploaded);
  }
  replace(){
    this.saveAcheivementList(this.acheivementSection.recommended);
  }
  isUser() {
    return this.authService.isUser();
  }
  isAdmin() {
    return this.authService.isAdmin();
  }


}

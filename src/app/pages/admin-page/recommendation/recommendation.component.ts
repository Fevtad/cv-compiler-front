import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { SectionFormat, UserCV } from '../objects/user-cv.object';
import { RecommendationService } from '../services/recommendation.service';
import { UserCVService } from '../services/user-cv.service';

@Component({
  selector: 'recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit {

  recommendations = [];
  successMessage = null;
  errorMessage = null;

  userCvList: UserCV[] = [];
  cvId = null;
  userId = null;
  educationSection = new SectionFormat();
  workSection = new SectionFormat();
  acheivementSection = new SectionFormat();
  skillSection = new SectionFormat();
  personalSection = new SectionFormat();
  referenceSection = new SectionFormat();

  constructor(private userCvService: UserCVService, private route: ActivatedRoute,
    private recommendationService: RecommendationService, private dialogService: NbDialogService) { }

  ngOnInit() {
    this.updateRecommendationComponent();
    this.userCvService.userCVListEmitter.subscribe(
      data => {
        this.userCvList = data;
        for (const section of this.userCvList) {
          if (section.section.name === 'education') {
            this.educationSection = section;
          } else if (section.section.name === 'skills') {
            this.skillSection = section;
          } else if (section.section.name === 'personalInfo') {
            this.personalSection = section;
          } else if (section.section.name === 'work') {
            this.workSection = section;
          } else if (section.section.name === 'achievement') {
            this.acheivementSection = section;
          } else if (section.section.name === 'reference') {
            this.referenceSection = section;
          }
        }
      },
    );
  }
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      { context: 'You\'re done!' });
  }
  updateRecommendationComponent() {
    this.route.params.subscribe(
      param => {
        this.userCvService.getUserCvDetail(param.id);
        this.cvId = param.id;
        this.userId = param.userId;
      },
    );
    setTimeout(() => {
      this.errorMessage = null;
      this.successMessage = null;
    }, 5000);
    this.recommendations = [];
  }
  updateRecommendation(section: any) {
    this.recommendations.push(section);
    console.log(this.recommendations)
    this.recommendationService.addRecommendation(this.cvId, this.recommendations);
    this.recommendationService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateRecommendationComponent();
    });

    this.recommendationService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error;
      }
    );
  }
  sendEmail() {
    this.recommendationService.sendEmail(this.userId);
    this.recommendationService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateRecommendationComponent();
    });

    this.recommendationService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error;
      }
    );
  }
  // onUpdateRecommendation(section: UserCV) {
  //   section.description = section.recommended;
  //   this.recommendations.push(section);
  //   this.updateRecommendation();
  // }
  // onupdateAllRecommendation() {
  //   for (const section of this.userCvList) {
  //     section.description = section.recommended;
  //     this.recommendations.push(section);
  //   }
  //   this.updateRecommendation();
  // }

  onUpdateCv(event) { }
  saveEducationVersion(educationSection: any) {
  }
  saveWorkVersion(workSection: any) {
  }
  saveAcheivementVersion(acheivementSection: any) {
  }
  saveSkillVersion(skillSection: any) {
  }
  savePersonalVersion(personalSection: any) {
  }
  saveReferenceVersion(referenceSection: any) {
  }

}

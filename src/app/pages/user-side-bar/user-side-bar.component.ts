import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-user-side-bar',
  templateUrl: './user-side-bar.component.html',
  styleUrls: ['./user-side-bar.component.css']
})
export class UserSideBarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  navigateToUpload(){
    this.router.navigateByUrl('pages/upload-cv/file-upload');
  }
  navigateToCvHistory(){
    this.router.navigateByUrl('pages/cv-history/cv-list');
  }
  navigateToCvTip(){
    this.router.navigateByUrl('pages/cv-tip/photo-tip');
  }
  navigateToTemplate(){
    this.router.navigateByUrl('pages/upload-cv/choose-template');
  }

}

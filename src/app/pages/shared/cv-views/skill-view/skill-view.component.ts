import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../custome-auth/custome-auth.service';
import { Skill } from '../../cv-forms/skill/skill.object';

@Component({
  selector: 'skill-view',
  templateUrl: './skill-view.component.html',
  styleUrls: ['./skill-view.component.scss']
})
export class SkillViewComponent implements OnInit {

  @Input() skillSection;
  @Input() cvId: string;
  @Output() saveSkillVersion: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateRecommendation: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateCv: EventEmitter<any> = new EventEmitter();
  selectedSkill = new Skill();
  successMessage = null;
  errorMessage = [];
  type = 'view';

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
    this.saveSkillVersion.emit(this.skillSection);
  }
  selectList(list: Skill) {
    this.selectedSkill = list;
    this.type = 'edit';
  }

  // when save is clicked
  saveSkill(Skill: Skill) {
    this.skillSection.description = Skill;
    if (this.isUser()) {
      // update cv for user
      this.onUpdateCv.emit(this.skillSection);
      // emit to the parent for version saving
      this.saveSkillVersion.emit(this.skillSection);
    } else if (this.isAdmin()){
       this.onUpdateRecommendation.emit(this.skillSection);
    }
    this.type = 'view';
  }
  keep(){
    this.saveSkill(this.skillSection.uploaded);
  }
  replace(){
    this.saveSkill(this.skillSection.recommended);
  }
  isArray(value){
    return Array.isArray(value);
  }
  isUser() {
    return this.authService.isUser();
  }
  isAdmin() {
    return this.authService.isAdmin();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EducationComponent } from './education/education.component';
import { WorkComponent } from './work/work.component';
import { SkillComponent } from './skill/skill.component';
import { PersonalComponent } from './personal/personal.component';
import { AcheivementComponent } from './acheivement/acheivement.component';
import { NbButtonModule, NbCardModule, NbIconModule, NbInputModule, NbSelectModule, NbStepperModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { ReferenceComponent } from './reference/reference.component';



@NgModule({
  declarations: [EducationComponent, WorkComponent, SkillComponent, PersonalComponent, AcheivementComponent, ReferenceComponent],
  imports: [
    CommonModule,
    FormsModule,
    NbCardModule,
    NbStepperModule,
    NbInputModule,
    NbButtonModule,
    NbIconModule,
    NbSelectModule,
  ],
  exports: [EducationComponent, WorkComponent, SkillComponent, PersonalComponent, AcheivementComponent, ReferenceComponent]
})
export class CvFormsModule { }

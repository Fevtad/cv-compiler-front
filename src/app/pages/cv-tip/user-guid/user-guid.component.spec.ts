import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGuidComponent } from './user-guid.component';

describe('UserGuidComponent', () => {
  let component: UserGuidComponent;
  let fixture: ComponentFixture<UserGuidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGuidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGuidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

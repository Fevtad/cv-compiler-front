import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomeAuthService } from '../custome-auth.service';
import { NewUser } from '../new-user.object';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {

  user = new NewUser();
  successMessage = null;
  errorMessage = null;
  constructor(private authService: CustomeAuthService, private router: Router,
    private route: ActivatedRoute) { }

  resetPass() {
    this.route.params.subscribe(
      param => {
        this.authService.resetPassword(param.token, this.user.password);
        this.authService.authSuccessEmitter.subscribe(
          data => {
            this.router.navigate(['/auth/login']);
          }
        );
        this.authService.authErrorEmitter.subscribe(
          error => {
            this.errorMessage = error;
          }
        );
      })
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoTipComponent } from './photo-tip.component';

describe('PhotoTipComponent', () => {
  let component: PhotoTipComponent;
  let fixture: ComponentFixture<PhotoTipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoTipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoTipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { EventEmitter, Injectable } from '@angular/core';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { HttpRequestService } from '../../../services/http-request.service';
import { PaginatedObject } from '../paginator/paginator.object';

@Injectable({
  providedIn: 'root'
})
export class PaginatorService {

  public PaginatedObjectListEmitter = new EventEmitter<PaginatedObject>();

    constructor(private httpService: HttpRequestService, private authService: CustomeAuthService) {
    }

    public getPaginatedData(fullUrl: string) {
        console.log(fullUrl);
        return this.httpService.sendCustomGetRequest(fullUrl, this.authService.getUserToken())
            .subscribe(
                data => {
                    this.processGetPaginatedData(data);
                },
                error => {
                    console.log(error);
                },
            );
    }

    private processGetPaginatedData(data) {
        console.log(data);
        if (data && data.disciples) {
            this.PaginatedObjectListEmitter.emit(data.disciples);
        }
    }
}

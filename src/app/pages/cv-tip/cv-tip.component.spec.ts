import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvTipComponent } from './cv-tip.component';

describe('CvTipComponent', () => {
  let component: CvTipComponent;
  let fixture: ComponentFixture<CvTipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvTipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvTipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

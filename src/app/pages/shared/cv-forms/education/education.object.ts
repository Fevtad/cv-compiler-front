export class Education {
    public degree: string;
    public field_of_study: string;
    public institute: string;
    public from: string;
    public to: string;
    constructor() {
        this.degree = null;
        this.field_of_study = null;
        this.institute = null;
        this.from = null;
        this.to = null;
    }
}

// export class EducationSection{
//     public uploaded: Section;
//     public recommended: any
// }
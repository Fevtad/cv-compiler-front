import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UploadCVRoutingModule } from './upload-cv-routing.module';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { ChooseTemplateComponent } from './choose-template/choose-template.component';
import { UploadCVComponent } from './upload-cv.component';
import { NbCardModule, NbButtonModule, NbIconModule, NbAlertModule, NbListModule } from '@nebular/theme';
import { UploadPhotoComponent } from './upload-photo/upload-photo.component';
import { CvFormsModule } from '../shared/cv-forms/cv-forms.module';
import { GenerateTemplateComponent } from './generate-template/generate-template.component';
import { NgxPrintModule } from 'ngx-print';

@NgModule({
  declarations: [FileUploadComponent, ChooseTemplateComponent, UploadCVComponent, UploadPhotoComponent, GenerateTemplateComponent],
  imports: [
    CommonModule,
    UploadCVRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbAlertModule,
    CvFormsModule,
    NbListModule,
    NgxPrintModule,
  ]
})
export class UploadCVModule { }

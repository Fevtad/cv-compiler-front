import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../custome-auth/custome-auth.service';
import { Education } from '../../cv-forms/education/education.object';

@Component({
  selector: 'education-view',
  templateUrl: './education-view.component.html',
  styleUrls: ['./education-view.component.scss']
})
export class EducationViewComponent implements OnInit {

  @Input() educationSection;
  @Input() cvId: string;
  @Output() saveEducationVersion: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateRecommendation: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateCv: EventEmitter<any> = new EventEmitter();
  selectedEducationList: Education[] = [];
  successMessage = null;
  errorMessage = [];
  type = 'view';

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
    this.saveEducationVersion.emit(this.educationSection);
  }
  selectList(list: Education[]) {
    this.selectedEducationList = list;
    this.type = 'edit';
  }

  // when save is clicked
  saveEducationList(educationList: Education[]) {
    this.educationSection.description = educationList;
    if (this.isUser()) {
      // update cv for user
      this.onUpdateCv.emit(this.educationSection);
      // emit to the parent for version saving
      this.saveEducationVersion.emit(this.educationSection);
    } else if (this.isAdmin()){
      console.log(this.educationSection)
       this.onUpdateRecommendation.emit(this.educationSection);
    }
    this.type = 'view';
  }
  keep(){
    this.saveEducationList(this.educationSection.uploaded);
  }
  replace(){
    this.saveEducationList(this.educationSection.recommended);
  }
  isUser() {
    return this.authService.isUser();
  }
  isAdmin() {
    return this.authService.isAdmin();
  }

}

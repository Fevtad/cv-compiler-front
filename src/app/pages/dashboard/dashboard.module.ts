import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { UserSideBarModule } from '../user-side-bar/user-side-bar.module';

@NgModule({
  imports: [
    UserSideBarModule,
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }

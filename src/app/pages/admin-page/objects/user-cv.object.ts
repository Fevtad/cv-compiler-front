import { Education } from '../../shared/cv-forms/education/education.object';
import { Personal } from '../../shared/cv-forms/personal/personal.object';
import { Reference } from '../../shared/cv-forms/reference/reference.object';
import { Skill } from '../../shared/cv-forms/skill/skill.object';
import { Work } from '../../shared/cv-forms/work/work.object';
import { CvSection } from '../../super-admin-page/cv-sections/cv-section.object';
import { User } from '../../super-admin-page/users/user.object';

export class UserCVPaginator {
    public current_page: number;
    public first_page_url: string;
    public from: number;
    public last_page: number;
    public last_page_url: string;
    public next_page_url: string;
    public path: string;
    public per_page: number;
    public prev_page_url: string;
    public to: number;
    public total: number;
    public data: UserCV[];

    constructor() {
        this.current_page = null;
        this.from = null;
        this.last_page = null;
        this.per_page = null;
        this.to = null;
        this.total = null;
        this.first_page_url = '';
        this.last_page_url = '';
        this.next_page_url = '';
        this.prev_page_url = '';
        this.path = '';

    }
}
export class UserCV {
    public _id: string;
    public userId: string;
    public user: User;
    public name: string;
    public section: CvSection;
    public sectionId: string;
    public uploaded: any;
    public recommended: any;
    public description: string;
    public createdAt: string;
    public updatedAt: string;
    constructor() {
        this._id = null;
        this.userId = null;
        this.user = new User();
        this.name = null;
        this.section = new CvSection();
        this.sectionId = null;
        this.uploaded = null;
        this.recommended = null;
        this.description = null;
        this.createdAt = null;
        this.updatedAt = null;
    }
}

export class UserForm {
    public education: Education[];
    public work: Work[];
    public reference: Reference[];
    public personalInfo: Personal;
    public achievement: [];
    public skill: Skill;
    constructor() {
        this.education = [];
        this.work = [];
        this.reference = [];
        this.personalInfo = new Personal();
        this.achievement = [];
        this.skill = new Skill();
    }
}

export class SectionFormat {
    public uploaded: any;
    public recommended: any;
    public sectionId: string;
    public name: string;
    public description: any;
    constructor() {
        this.uploaded = [];
        this.recommended = [];
        this.sectionId = null;
        this.name = null;
        this.description = null;
    }
}

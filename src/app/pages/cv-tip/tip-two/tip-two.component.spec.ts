import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipTwoComponent } from './tip-two.component';

describe('TipTwoComponent', () => {
  let component: TipTwoComponent;
  let fixture: ComponentFixture<TipTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

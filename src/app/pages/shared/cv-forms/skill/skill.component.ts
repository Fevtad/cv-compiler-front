import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CvFormService } from '../services/cv-form.service';
import { Skill } from './skill.object';

@Component({
  selector: 'skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit {

  @Output() onAddSkill: EventEmitter<any> = new EventEmitter();
  skill: Skill;
  hardArr = [];
  softArr = [];
  languageArr = [];

  constructor(private cvFormService: CvFormService) { }

  ngOnInit() {
    this.skill = this.cvFormService.getSkill();
    this.skill.language = [
      {'type': '', 'level': ''},
      {'type': '', 'level': ''},
      {'type': '', 'level': ''}
    ];
  }

  submitSkill(){
    let i = this.skill.language.length;
    while (i--) {
      if (this.skill.language[i].type === '' || this.skill.language[i].level === '') {
        this.skill.language.splice(i, 1);
      }
    }
    this.cvFormService.saveSkill(this.skill);
    this.onAddSkill.emit(this.skill);
  }
  addSoftSkill(){
    if (this.softArr.length < 4){
      this.softArr.length += 1;
    }
  }
  addLanguage(){
    if (this.languageArr.length < 3){
      this.languageArr.length += 1;
    }
  }
  addHardSkill(){
    if (this.hardArr.length < 4){
      this.hardArr.length += 1;
    }
  }

}

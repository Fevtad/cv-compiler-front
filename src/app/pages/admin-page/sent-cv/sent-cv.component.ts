import { Component, OnInit } from '@angular/core';
import { UserCV } from '../objects/user-cv.object';
import { UserCVService } from '../services/user-cv.service';

@Component({
  selector: 'ngx-sent-cv',
  templateUrl: './sent-cv.component.html',
  styleUrls: ['./sent-cv.component.scss']
})
export class SentCvComponent implements OnInit {

  sentUserCVList: UserCV[] = [];

  constructor(private cvService: UserCVService) { }

  ngOnInit() {
    this.updateSentCvComponent();
    this.cvService.sentUserCVListEmitter.subscribe(
      data => {
        this.sentUserCVList = data;
      }
    );
  }

  updateSentCvComponent() {
    this.cvService.getAllSentUserCv();
  }
}

import { Component, OnInit } from '@angular/core';
import { UploadCvService } from '../upload-cv.service';
import { Router } from '@angular/router';
import { UserForm } from '../../admin-page/objects/user-cv.object';
import { Education } from '../../shared/cv-forms/education/education.object';
import { Work } from '../../shared/cv-forms/work/work.object';
import { Skill } from '../../shared/cv-forms/skill/skill.object';
import { Personal } from '../../shared/cv-forms/personal/personal.object';
import { CvSection, FormSection } from '../../super-admin-page/cv-sections/cv-section.object';
import { CvSectionService } from '../../super-admin-page/services/cv-section.service';
import { Reference } from '../../shared/cv-forms/reference/reference.object';

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  errorMessage = null;
  successMessage = null;
  menues = ['Personal Detail', 'Education', 'Work Experiance', 'Achievement', 'Skill', 'Reference'];
  view = 'Personal Detail';
  userForm = new UserForm();
  newForm = [];
  formSection = new FormSection();
  sections: CvSection[] = [];
  hasUploaded = false;

  constructor(private uploadCvService: UploadCvService, private cvService: CvSectionService,
    private router: Router) { }

  ngOnInit() {
    this.uploadCvService.hasUploaded();
    this.uploadCvService.hasUploadedEmitter.subscribe(
      data => {
        this.hasUploaded = data;
      }
    );
    this.cvService.getAllCvSections();
    this.cvService.cvSectionListEmitter.subscribe(
      data => {
        this.sections = data;
      }
    );
  }
  updateFileUploadComponent() {
    setTimeout(() => {
      this.errorMessage = null;
      this.successMessage = null;
    }, 5000);
    this.newForm = [];
  }
  navigateToDashboard() {
    this.router.navigate(['/pages/dashboard']);
  }
  onAddEducation(education: Education[]) {
    this.userForm.education = education;
  }
  onAddWork(work: Work[]) {
    this.userForm.work = work;
  }
  onAddAcheivement(acheivement: []) {
    this.userForm.achievement = acheivement;
  }
  onAddSkill(skill: Skill) {
    this.userForm.skill = skill;
  }
  onAddPersonal(personal: Personal) {
    this.userForm.personalInfo = personal;
  }
  onAddReference(reference: Reference[]) {
    this.userForm.reference = reference;
  }
  readyToSend() {
    if (this.userForm.education[0] && this.userForm.work[0] && this.userForm.personalInfo.first_name
      && this.userForm.skill.language[0] && this.userForm.reference[0]) {
      return true;
    }
    return false;
  }
  submitForm() {

    for (let section of this.sections) {
      if (section.name === 'education') {
        this.formSection.description = this.userForm.education;
        this.formSection.name = section.name;
        this.formSection.sectionId = section._id;
        this.newForm.push(this.formSection);
        this.formSection = new FormSection();
      } else if (section.name === 'skills') {
        this.formSection.description = this.userForm.skill;
        this.formSection.name = section.name;
        this.formSection.sectionId = section._id;
        this.newForm.push(this.formSection);
        this.formSection = new FormSection();
      } else if (section.name === 'achievement') {
        this.formSection.description = this.userForm.achievement;
        this.formSection.name = section.name;
        this.formSection.sectionId = section._id;
        this.newForm.push(this.formSection);
        this.formSection = new FormSection();
      } else if (section.name === 'personalInfo') {
        this.formSection.description = this.userForm.personalInfo;
        this.formSection.name = section.name;
        this.formSection.sectionId = section._id;
        this.newForm.push(this.formSection);
        this.formSection = new FormSection();
      } else if (section.name === 'reference') {
        this.formSection.description = this.userForm.reference;
        this.formSection.name = section.name;
        this.formSection.sectionId = section._id;
        this.newForm.push(this.formSection);
        this.formSection = new FormSection();
      } else {
        this.formSection.description = this.userForm.work;
        this.formSection.name = section.name;
        this.formSection.sectionId = section._id;
        this.newForm.push(this.formSection);
      }
    }

    this.uploadCvService.uploadForm(this.newForm);
    this.uploadCvService.responseEmitter.subscribe(
      data => {
        this.router.navigate(['/pages/upload-cv/photo-upload']);
      }
    );
    this.uploadCvService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error;
        this.updateFileUploadComponent();
      }
    );
  }

  changeForm(menu: string) {
    this.view = menu;
  }

}

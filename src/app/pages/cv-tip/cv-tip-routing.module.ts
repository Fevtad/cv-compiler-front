import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipComponent } from './tip/tip.component';
import { UserGuidComponent } from './user-guid/user-guid.component';
import { CvTipComponent } from './cv-tip.component';
import { PhotoTipComponent } from './photo-tip/photo-tip.component';

const routes: Routes = [
  {
    path: '',
    component: CvTipComponent,
    children: [
      {
        path: 'tip',
        component: TipComponent,
      },
      {
        path: 'photo-tip',
        component: PhotoTipComponent,
      },
      {
        path: 'user-guid',
        component: UserGuidComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CvTipRoutingModule { }

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomeAuthRoutingModule } from './custome-auth-routing.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NbAlertModule, NbInputModule, NbButtonModule, NbCheckboxModule, NbIconModule } from '@nebular/theme';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { NbAuthModule } from '@nebular/auth';
import { VerifyComponent } from './verify/verify.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RequestPasswordComponent } from './request-password/request-password.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, VerifyComponent, ResetPasswordComponent, RequestPasswordComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    CustomeAuthRoutingModule,
    NbIconModule,
    Ng2TelInputModule,
    NbAuthModule,
    // NbAuthModule.forRoot({
    //   strategies: [
    //     NbPasswordAuthStrategy.setup({
    //       name: 'email',
    //       token: {
    //         class: NbAuthJWTToken,
    //         key: 'token', // this parameter tells where to look for the token
    //       },
    //       baseEndpoint: 'localhost:3000',
    //       login: {
    //         redirect: {
    //           success: '/pages/dashboard',
    //           failure: null, // stay on the same page
    //         },
    //         endpoint: '/user/login',
    //         method: 'post',
    //       },
    //       register: {
    //         redirect: {
    //           success: '/pages/dashboard',
    //           failure: null, // stay on the same page
    //         },
    //         endpoint: '/user/signup',
    //         method: 'post',
    //       },
    //     }),
    //   ],
    //   forms: {
    //     login: {
    //       redirectDelay: 0,
    //       showMessages: {
    //         success: true,
    //       },
    //     },
    //     register: {
    //       redirectDelay: 0,
    //       showMessages: {
    //         success: true,
    //       },
    //     },
    //   },
    // }),
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class CustomeAuthModule { }

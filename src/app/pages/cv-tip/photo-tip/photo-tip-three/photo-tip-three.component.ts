import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'photo-tip-three',
  templateUrl: './photo-tip-three.component.html',
  styleUrls: ['./photo-tip-three.component.scss']
})
export class PhotoTipThreeComponent implements OnInit {

  @Output() previous: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  previousPhoto(){
    this.previous.emit(true);
  }

}

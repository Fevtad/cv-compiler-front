import { EventEmitter, Injectable } from '@angular/core';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { HttpRequestService } from '../../../services/http-request.service';
import { UserCV } from '../objects/user-cv.object';

@Injectable({
  providedIn: 'root'
})
export class UserCVService {

  token = this.authSerice.getUserToken();
  paginate_count = this.httprequest.paginate_count;
  newUserCVListEmitter = new EventEmitter<UserCV>();
  inProgressUserCVListEmitter = new EventEmitter<UserCV>();
  sentUserCVListEmitter = new EventEmitter<UserCV>();
  userCVListEmitter = new EventEmitter<UserCV>();
  responseEmitter = new EventEmitter();
  errorEmitter = new EventEmitter();

  constructor(private httprequest: HttpRequestService, private authSerice: CustomeAuthService) { }

  getAllNewUserCv() {
    this.httprequest.sendGetRequest('v1/admin/assigned-cv/new?skip=0&limit=10', this.token).subscribe(
        data => {
          this.processGetAllNewUserCV(data);
        },
        error => {
          console.log('Something Went Wrong!!!', error);
        },
    );
  }
  processGetAllNewUserCV(data: any) {
    if (data) {
      this.newUserCVListEmitter.emit(data);
    }
  }
  getAllInProgressUserCv() {
    this.httprequest.sendGetRequest('v1/admin/assigned-cv/onprogress?skip=0&limit=10', this.token).subscribe(
        data => {
          this.processGetAllInProgressUserCV(data);
        },
        error => {
          console.log('Something Went Wrong!!!', error);
        },
    );
  }
  processGetAllInProgressUserCV(data: any) {
    if (data) {
      this.inProgressUserCVListEmitter.emit(data);
    }
  }
  getAllSentUserCv() {
    this.httprequest.sendGetRequest('v1/admin/assigned-cv/sent?skip=0&limit=10', this.token).subscribe(
        data => {
          this.processGetAllSentUserCV(data);
        },
        error => {
          console.log('Something Went Wrong!!!', error);
        },
    );
  }
  processGetAllSentUserCV(data: any) {
    if (data) {
      this.sentUserCVListEmitter.emit(data);
    }
  }
  getUserCvDetail(id: string) {
    this.httprequest.sendGetRequest('v1/admin/detailedcv/' + id, this.token).subscribe(
        data => {
          this.processGetUserCvDetail(data);
        },
        error => {
          console.log('Something Went Wrong!!!', error);
        },
    );
  }
  processGetUserCvDetail(data: any) {
    console.log(data);
    if (data.sections) {
      this.userCVListEmitter.emit(data.sections);
    }
  }
}

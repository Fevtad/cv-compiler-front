import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllCvComponent } from './all-cv/all-cv.component';
import { RecommendationComponent } from './recommendation/recommendation.component';

const routes: Routes = [
  {
    path: '',
    // component: AdminPageComponent,
    children: [
      {
        path: 'recommendation/:userId/:id',
        component: RecommendationComponent,
      },
      {
        path: 'all-cv',
        component: AllCvComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPageRoutingModule { }

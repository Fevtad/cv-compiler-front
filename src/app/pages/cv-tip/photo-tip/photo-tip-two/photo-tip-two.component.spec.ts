import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoTipTwoComponent } from './photo-tip-two.component';

describe('PhotoTipTwoComponent', () => {
  let component: PhotoTipTwoComponent;
  let fixture: ComponentFixture<PhotoTipTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoTipTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoTipTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

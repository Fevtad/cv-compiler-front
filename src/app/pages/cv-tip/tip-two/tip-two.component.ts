import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'tip-two',
  templateUrl: './tip-two.component.html',
  styleUrls: ['./tip-two.component.scss']
})
export class TipTwoComponent implements OnInit {

  @Output() next: EventEmitter<any> = new EventEmitter();
  @Output() previous: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  nextTip() {
    this.next.emit(true);
  }
  previousTip() {
    this.previous.emit(true);
  }

}

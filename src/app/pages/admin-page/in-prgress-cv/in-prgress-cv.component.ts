import { Component, OnInit } from '@angular/core';
import { UserCV } from '../objects/user-cv.object';
import { UserCVService } from '../services/user-cv.service';

@Component({
  selector: 'ngx-in-prgress-cv',
  templateUrl: './in-prgress-cv.component.html',
  styleUrls: ['./in-prgress-cv.component.scss']
})
export class InPrgressCvComponent implements OnInit {

  inProgressUserCVList: UserCV[] = [];

  constructor(private cvService: UserCVService) { }

  ngOnInit() {
    this.updateInProgressCvComponent();
    this.cvService.inProgressUserCVListEmitter.subscribe(
      data => {
        this.inProgressUserCVList = data;
      }
    );
  }

  updateInProgressCvComponent() {
    this.cvService.getAllInProgressUserCv();
  }

}

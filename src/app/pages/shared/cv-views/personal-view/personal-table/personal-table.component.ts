import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../../custome-auth/custome-auth.service';
import { Personal } from '../../../cv-forms/personal/personal.object';

@Component({
  selector: 'personal-table',
  templateUrl: './personal-table.component.html',
  styleUrls: ['./personal-table.component.scss']
})
export class PersonalTableComponent implements OnInit {

  @Input() personal: Personal;
  @Input() type: string;
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
  }
  cancel(){
    this.onCancel.emit(true);
  }
  save(){
    this.onSave.emit(this.personal);
  }
  isUser(){
    return this.authService.isUser();
  }


}

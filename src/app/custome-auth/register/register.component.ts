import { Component, OnInit } from '@angular/core';
import { NewUser } from '../new-user.object';
import { CustomeAuthService } from '../custome-auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  successMessage: any = '';
  strategy: string = '';
  user = new NewUser();
  submitted: boolean = false;
  errorMessage = null;
  phoneValid = true;

  constructor(private authService: CustomeAuthService, private router: Router) { }

  ngOnInit() {
  }
  register() {
    this.user.role = 'user';
    this.authService.register(this.user);
    this.authService.authSuccessEmitter.subscribe(
      data => {
        this.successMessage = data.msg;
        window.scroll(0, 0);
      }
    );
    this.authService.authErrorEmitter.subscribe(
      error => {
        this.errorMessage = error;
      }
    );
  }
  hasError(event) {
    this.phoneValid = event;
  }
  getNumber(event) {
    this.user.phoneNumber = event;
  }


}

import { Component, OnInit } from '@angular/core';
import { UserCV } from '../objects/user-cv.object';
import { UserCVService } from '../services/user-cv.service';

@Component({
  selector: 'ngx-new-cv',
  templateUrl: './new-cv.component.html',
  styleUrls: ['./new-cv.component.scss']
})
export class NewCvComponent implements OnInit {

  newUserCVList: UserCV[] = [];

  constructor(private cvService: UserCVService) { }

  ngOnInit() {
    this.updateNewCvComponent();
    this.cvService.newUserCVListEmitter.subscribe(
      data => {
        this.newUserCVList = data;
      }
    );
  }

  updateNewCvComponent() {
    this.cvService.getAllNewUserCv();
  }

}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'tip-three',
  templateUrl: './tip-three.component.html',
  styleUrls: ['./tip-three.component.scss']
})
export class TipThreeComponent implements OnInit {

  @Output() next: EventEmitter<any> = new EventEmitter();
  @Output() previous: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  nextTip() {
    
    this.next.emit(true);
  }
  previousTip() {
    this.previous.emit(true);
  }
}

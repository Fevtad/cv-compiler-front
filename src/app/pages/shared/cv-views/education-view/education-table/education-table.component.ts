import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../../custome-auth/custome-auth.service';
import { Education } from '../../../cv-forms/education/education.object';

@Component({
  selector: 'education-table',
  templateUrl: './education-table.component.html',
  styleUrls: ['./education-table.component.scss']
})
export class EducationTableComponent implements OnInit {

  @Input() educationList: Education[];
  @Input() type: string;
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
  }
  cancel(){
    this.onCancel.emit(true);
  }
  save(){
    this.onSave.emit(this.educationList);
  }
  isUser(){
    return this.authService.isUser();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoTipThreeComponent } from './photo-tip-three.component';

describe('PhotoTipThreeComponent', () => {
  let component: PhotoTipThreeComponent;
  let fixture: ComponentFixture<PhotoTipThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoTipThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoTipThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class Reference {
    public first_name: string;
    public last_name: string;
    public company: string;
    public email: string;
    public phone: string;
    public profession: string;
    constructor() {
        this.first_name = null;
        this.last_name = null;
        this.company = null;
        this.email = null;
        this.phone = null;
        this.profession = null;
    }
}

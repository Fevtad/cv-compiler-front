import { Injectable, EventEmitter } from '@angular/core';
import { HttpRequestService } from '../../services/http-request.service';
import { CustomeAuthService } from '../../custome-auth/custome-auth.service';
import { UserForm } from '../admin-page/objects/user-cv.object';

@Injectable({
  providedIn: 'root'
})
export class UploadCvService {

  token = this.authSerice.getUserToken();
  responseEmitter = new EventEmitter();
  errorEmitter = new EventEmitter();
  sectionList = new EventEmitter();
  hasUploadedEmitter = new EventEmitter<boolean>();

  constructor(private httprequest: HttpRequestService, private authSerice: CustomeAuthService) { }

  uploadImage(file: any) {
    const formData: FormData = new FormData();
    formData.append('cvprofile', file);
    return this.httprequest.sendFilePostRequest('v1/user/cv/upload/profileimage', formData, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.errors.msg);
        }
      );
  }
  uploadForm(sections: any) {
    console.log(sections);
    return this.httprequest.sendPostRequest('v1/user/cvsections', { sections }, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.msg);
        }
      );
  }
  getCvSections() {
    return this.httprequest.sendGetRequest('v1/common/sections', this.token)
      .subscribe(
        data => {
          this.processCvSections(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.errors.msg);
        }
      );
  }
  processCvSections(data: any) {
    if (data) {
      this.sectionList.emit(data);
    }
  }
  hasUploaded() {
    return this.httprequest.sendGetRequest('v1/user/iscvuploaded', this.token)
      .subscribe(
        data => {
          this.processhasUploaded(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.errors.msg);
        }
      );
  }
  processhasUploaded(data: any) {
    if (data) {
      this.hasUploadedEmitter.emit(data.status);
    }
  }
}


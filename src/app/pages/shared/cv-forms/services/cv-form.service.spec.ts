import { TestBed } from '@angular/core/testing';

import { CvFormService } from './cv-form.service';

describe('CvFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CvFormService = TestBed.get(CvFormService);
    expect(service).toBeTruthy();
  });
});

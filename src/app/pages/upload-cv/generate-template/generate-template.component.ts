import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { UserForm } from '../../admin-page/objects/user-cv.object';
import { CvService } from '../../cv-history/services/cv.service';

@Component({
  selector: 'generate-template',
  templateUrl: './generate-template.component.html',
  styleUrls: ['./generate-template.component.scss']
})
export class GenerateTemplateComponent implements OnInit {

  educationSection;
  workSection;
  achievementSection;
  skillSection;
  personalSection;

  userForm = new UserForm;
  imageURL = null;

  constructor(private cvService: CvService, private authService: CustomeAuthService,
              private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.route.params.subscribe(
      param => {
        if (param.type === 'latest'){
          this.cvService.getUserCvDetail(this.authService.getId());
        } else if (param.type === 'history'){
            this.cvService.getCvHistory(param.id);
        }
      }
    );
    this.cvService.userCVListEmitter.subscribe(
      datas => {
        if (datas) {
          console.log(datas)
          this.imageURL = 'http://stg.cvcomp.et6bw.gebeya.co' + datas.cvProfileImage;
          // this.imageURL = 'http://localhost:3000' + datas.cvProfileImage;
          for (const section of datas.sections) {
            if (section.name === 'education') {
              this.userForm.education = section.uploaded;
            } else if (section.name === 'work') {
              this.userForm.work = section.uploaded;
            } else if (section.name === 'achievement') {
              this.userForm.achievement = section.uploaded;
            } else if (section.name === 'personalInfo') {
              this.userForm.personalInfo = section.uploaded;
            } else if (section.name === 'skills') {
              this.userForm.skill = section.uploaded;
            } else if (section.name === 'reference') {
              this.userForm.reference = section.uploaded;
            }
          }
        }
      }
    );
  }
  back(){
    this.location.back();
  }

}

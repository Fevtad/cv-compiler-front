import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  private rootURl = 'https://stg.cvcomp.et6bw.gebeya.co/';
  // rootURl = 'http://localhost:3000/';
  paginate_count = 10;

  constructor(private httpRequest: HttpClient) { }

  public sendGetRequest(routeName, token) {
    const request_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    return this.httpRequest.get(this.rootURl + routeName, { headers: request_header });
  }
  public sendPostRequest(routeName, body, token) {
    const request_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    return this.httpRequest.post(this.rootURl + routeName, body, { headers: request_header });
  }
  public sendFilePostRequest(routeName, body, token) {
    const request_header = new HttpHeaders({
      // 'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>',
      'enctype': "multipart/form-data",
      'Authorization': 'Bearer ' + token
    });
    return this.httpRequest.patch(this.rootURl + routeName, body, { headers: request_header });
  }
  public sendPutRequest(routeName, body, token) {
    const request_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    return this.httpRequest.put(this.rootURl + routeName, body, { headers: request_header });
  }
  public sendPatchRequest(routeName, body, token) {
    const request_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    return this.httpRequest.patch(this.rootURl + routeName, body, { headers: request_header });
  }
  public sendDeleteRequest(routeName, token) {
    const request_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    return this.httpRequest.delete(this.rootURl + routeName, { headers: request_header });
  }
  public sendCustomGetRequest(full_url, token) {
    const request_header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    return this.httpRequest.get(full_url, { headers: request_header });
  }
  public sendCustomePostRequest(routeName, body) {
    return this.httpRequest.post(this.rootURl + routeName, body);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoTipOneComponent } from './photo-tip-one.component';

describe('PhotoTipOneComponent', () => {
  let component: PhotoTipOneComponent;
  let fixture: ComponentFixture<PhotoTipOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoTipOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoTipOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadCVComponent } from './upload-cv.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { ChooseTemplateComponent } from './choose-template/choose-template.component';
import { UploadPhotoComponent } from './upload-photo/upload-photo.component';
import { GenerateTemplateComponent } from './generate-template/generate-template.component';

const routes: Routes = [
  {
    path: '',
    component: UploadCVComponent,
    children: [
      {
        path: 'file-upload',
        component: FileUploadComponent,
      },
      {
        path: 'photo-upload',
        component: UploadPhotoComponent,
      },
      {
        path: 'choose-template',
        component: ChooseTemplateComponent,
      },
      {
        path: 'generate-template/:type/:id',
        component: GenerateTemplateComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadCVRoutingModule { }

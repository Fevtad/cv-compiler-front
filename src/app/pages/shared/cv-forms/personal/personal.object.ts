export class Personal {
    public first_name: string;
    public last_name: string;
    public personal_statement: string;
    public application_job_title: string;
    public phone: string;
    public postal_code: string;
    public email: string;
    public nationality: string;
    public country: string;
    public city: string;
    public birth_date: string;
    public links: Link[];
    constructor() {
        this.first_name = null;
        this.last_name = null;
        this.personal_statement = null;
        this.application_job_title = null;
        this.phone = null;
        this.postal_code = null;
        this.email = null;
        this.nationality = null;
        this.country = null;
        this.city = null;
        this.birth_date = null;
        this.links = [];
    }
}

export class Link{
    public type: string;
    public link: string;
    constructor(){
        this.type = null;
        this.link = null;
    }
}
import { Component, OnInit } from '@angular/core';
import { Swiper } from 'swiper/bundle';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  mySwiper: any;

  constructor() { }

  ngOnInit() {
    this.mySwiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      initialSlide: 2,
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });
  }

}

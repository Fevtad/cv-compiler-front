import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CvFormService } from '../services/cv-form.service';
import { Reference } from './reference.object';

@Component({
  selector: 'reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss']
})
export class ReferenceComponent implements OnInit {

  reference = new Reference();
  @Output() onAddReference: EventEmitter<any> = new EventEmitter();
  referenceList: Reference[] = [];
  isDone = false;

  constructor(private cvFormService: CvFormService) { }

  ngOnInit() {
    this.referenceList = this.cvFormService.getReference();
    for (const Reference of this.referenceList){
      this.reference = Reference;
    }
  }

  submitReference() {
    this.referenceList.push(this.reference);
  }
  done() {
    this.cvFormService.saveReference(this.referenceList);
    this.onAddReference.emit(this.referenceList);
    this.isDone = true;
  }

  resetReference() {
    this.reference = new Reference();
  }

}

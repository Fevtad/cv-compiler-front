import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminPageRoutingModule } from './admin-page-routing.module';
import { AdminPageComponent } from './admin-page.component';
import { NewCvComponent } from './new-cv/new-cv.component';
import { InPrgressCvComponent } from './in-prgress-cv/in-prgress-cv.component';
import { SentCvComponent } from './sent-cv/sent-cv.component';
import { RecommendationComponent } from './recommendation/recommendation.component';
import { NbAlertModule, NbButtonModule, NbCardModule, NbIconModule, NbListModule } from '@nebular/theme';
import { CKEditorModule } from 'ng2-ckeditor';
import { AllCvComponent } from './all-cv/all-cv.component';
import { FormsModule } from '@angular/forms';
import { CvFormsModule } from '../shared/cv-forms/cv-forms.module';
import { CvViewsModule } from '../shared/cv-views/cv-views.module';

@NgModule({
  declarations: [AdminPageComponent, NewCvComponent, InPrgressCvComponent, SentCvComponent, RecommendationComponent, AllCvComponent],
  imports: [
    CommonModule,
    NbCardModule,
    CKEditorModule,
    FormsModule,
    NbButtonModule,
    NbAlertModule,
    NbListModule,
    CvFormsModule,
    CvViewsModule,
    NbIconModule,
    AdminPageRoutingModule,
  ]
})
export class AdminPageModule { }

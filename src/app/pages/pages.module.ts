import { NgModule } from '@angular/core';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { AccountSettingModule } from './account-setting/account-setting.module';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    AccountSettingModule,
  ],
  declarations: [
    PagesComponent,
    UnauthorizedComponent,
  ],
})
export class PagesModule {
}

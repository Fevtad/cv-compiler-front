import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { NbLayoutModule, NbCardModule, NbButtonModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { ThemeModule } from '../@theme/theme.module';
import { HomeFooterComponent } from './home-footer/home-footer.component';

@NgModule({
  declarations: [HomeComponent, HomeFooterComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    RouterModule,
    NbLayoutModule,
    ThemeModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbInputModule,
  ],
})
export class HomeModule { }

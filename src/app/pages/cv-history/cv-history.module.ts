import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CvHistoryRoutingModule } from './cv-history-routing.module';
import { CvHistoryComponent } from './cv-history.component';
import { CvListComponent } from './cv-list/cv-list.component';
import { CvDetailComponent } from './cv-detail/cv-detail.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { FormsModule } from '@angular/forms';
import { NbCardModule, NbButtonModule, NbPopoverModule, NbCheckboxModule, NbAlertModule, NbIconModule } from '@nebular/theme';
import { CvViewsModule } from '../shared/cv-views/cv-views.module';
import { TimeAgoPipe } from 'time-ago-pipe';

@NgModule({
  declarations: [CvHistoryComponent, CvListComponent, CvDetailComponent, TimeAgoPipe],
  imports: [
    CommonModule,
    NbCardModule,
    CKEditorModule,
    FormsModule,
    NbButtonModule,
    NbPopoverModule,
    NbCheckboxModule,
    NbAlertModule,
    CvViewsModule,
    NbIconModule,
    CvHistoryRoutingModule,
  ]
})
export class CvHistoryModule { }

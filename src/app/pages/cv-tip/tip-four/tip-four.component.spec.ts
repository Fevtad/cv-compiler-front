import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipFourComponent } from './tip-four.component';

describe('TipFourComponent', () => {
  let component: TipFourComponent;
  let fixture: ComponentFixture<TipFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

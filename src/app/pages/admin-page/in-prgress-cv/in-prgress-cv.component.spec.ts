import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InPrgressCvComponent } from './in-prgress-cv.component';

describe('InPrgressCvComponent', () => {
  let component: InPrgressCvComponent;
  let fixture: ComponentFixture<InPrgressCvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InPrgressCvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InPrgressCvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../../custome-auth/custome-auth.service';
import { Skill } from '../../../cv-forms/skill/skill.object';

@Component({
  selector: 'skill-table',
  templateUrl: './skill-table.component.html',
  styleUrls: ['./skill-table.component.scss']
})
export class SkillTableComponent implements OnInit {

  @Input() skill: Skill;
  @Input() type: string;
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
  }
  cancel(){
    this.onCancel.emit(true);
  }
  save(){
    this.onSave.emit(this.skill);
  }
  onChangeSoft(event, index){
    this.skill.soft_skill[index] = event.target.value;
  }
  onChangeHard(event, index){
    this.skill.hard_skill[index] = event.target.value;
  }
  isUser(){
    return this.authService.isUser();
  }
}

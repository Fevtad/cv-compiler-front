import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountSettingComponent } from './account-setting.component';
import { NbAccordionModule, NbInputModule, NbButtonModule, NbIconModule, NbAlertModule } from '@nebular/theme';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AccountSettingComponent],
  imports: [
    CommonModule,
    FormsModule,
    NbAccordionModule,
    NbInputModule,
    NbButtonModule,
    NbIconModule,
    NbAlertModule,
    RouterModule,
  ],
})
export class AccountSettingModule { }

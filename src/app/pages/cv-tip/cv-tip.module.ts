import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CvTipRoutingModule } from './cv-tip-routing.module';
import { CvTipComponent } from './cv-tip.component';
import { TipComponent } from './tip/tip.component';
import { UserGuidComponent } from './user-guid/user-guid.component';
import { NbCardModule, NbIconModule, NbButtonModule, NbStepperModule } from '@nebular/theme';
import { TipOneComponent } from './tip-one/tip-one.component';
import { TipTwoComponent } from './tip-two/tip-two.component';
import { TipThreeComponent } from './tip-three/tip-three.component';
import { TipFourComponent } from './tip-four/tip-four.component';
import { TipFiveComponent } from './tip-five/tip-five.component';
import { PhotoTipComponent } from './photo-tip/photo-tip.component';
import { PhotoTipOneComponent } from './photo-tip/photo-tip-one/photo-tip-one.component';
import { PhotoTipTwoComponent } from './photo-tip/photo-tip-two/photo-tip-two.component';
import { PhotoTipThreeComponent } from './photo-tip/photo-tip-three/photo-tip-three.component';

@NgModule({
  declarations: [CvTipComponent, TipComponent, UserGuidComponent, TipOneComponent, TipTwoComponent, TipThreeComponent, TipFourComponent, TipFiveComponent, PhotoTipComponent, PhotoTipOneComponent, PhotoTipTwoComponent, PhotoTipThreeComponent],
  imports: [
    CommonModule,
    CvTipRoutingModule,
    NbCardModule,
    NbIconModule,
    NbButtonModule,
    NbStepperModule,
  ]
})
export class CvTipModule { }

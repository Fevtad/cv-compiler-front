import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CvFormService } from '../services/cv-form.service';

@Component({
  selector: 'acheivement',
  templateUrl: './acheivement.component.html',
  styleUrls: ['./acheivement.component.scss']
})
export class AcheivementComponent implements OnInit {

  @Output() onAddAcheivement: EventEmitter<any> = new EventEmitter();
  acheivement;
  Arr = [];

  constructor(private cvFormService: CvFormService) { }

  ngOnInit() {
    this.acheivement = this.cvFormService.getAchievement();
  }

  submitAcheivement() {
    this.cvFormService.saveAchievement(this.acheivement);
    this.onAddAcheivement.emit(this.acheivement);
  }
  addAcheivement() {
    if (this.Arr.length < 4) {
      this.Arr.length += 1;
    }
  }
}

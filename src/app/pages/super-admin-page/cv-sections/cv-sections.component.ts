import { Component, OnInit, TemplateRef } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { CvSectionService } from '../services/cv-section.service';
import { CvSection } from './cv-section.object';

@Component({
  selector: 'ngx-cv-sections',
  templateUrl: './cv-sections.component.html',
  styleUrls: ['./cv-sections.component.scss']
})
export class CvSectionsComponent implements OnInit {

  successMessage = null;
  errorMessage = [];
  sectionList: CvSection[] = [];
  newSection = new CvSection();
  selectedSection = new CvSection();

  constructor(private cvSetionService: CvSectionService, private dialogService: NbDialogService) { }

  ngOnInit() {
    this.updateCvSectionsComponent();
    this.cvSetionService.cvSectionListEmitter.subscribe(
      data => {
        this.sectionList = data;
      },
    );
  }
  updateCvSectionsComponent() {
    this.cvSetionService.getAllCvSections();
    setTimeout(() => {
      this.errorMessage = [];
      this.successMessage = null;
    }, 5000);
  }
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      { context: 'You\'re done!' });
  }

  onNewCvSectionAdded() {
    this.cvSetionService.addCvSection(this.newSection);
    this.cvSetionService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateCvSectionsComponent();
    });

    this.cvSetionService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error.slice();
      }
    );
  }

  selectCvSection(section: CvSection) {
    this.selectedSection = section;
  }

  deleteCvSection() {
    this.cvSetionService.deleteCvSection(this.selectedSection._id);
    this.cvSetionService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateCvSectionsComponent();
    });
    this.cvSetionService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error.slice();
      },
    );
  }

}

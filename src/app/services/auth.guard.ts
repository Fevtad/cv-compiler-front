import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators';
import { CustomeAuthService } from '../custome-auth/custome-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: CustomeAuthService, private router: Router) {
  }

  canActivate() {
        if (this.authService.isAuthenticated()) {
          return true;
        }else {
          this.router.navigate(['auth/login']);
          return false;
        }
  }
}

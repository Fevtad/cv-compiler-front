import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcheivementViewComponent } from './acheivement-view.component';

describe('AcheivementViewComponent', () => {
  let component: AcheivementViewComponent;
  let fixture: ComponentFixture<AcheivementViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcheivementViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcheivementViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSideBarComponent } from './user-side-bar.component';
import { NbIconModule } from '@nebular/theme';

@NgModule({
  declarations: [UserSideBarComponent],
  imports: [
    CommonModule,
    NbIconModule,
  ],
  exports: [UserSideBarComponent],
})
export class UserSideBarModule { }

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CustomeAuthService } from '../custome-auth/custome-auth.service';

@Injectable({
  providedIn: 'root'
})
export class SuperAdminGuard implements CanActivate {
  constructor(private authService: CustomeAuthService, private router: Router) {
  }

  canActivate() {
        if (this.authService.isSuperAdmin()) {
          return true;
        }else {
          this.router.navigate(['pages/unauthorized']);
          return false;
        }
  }
}

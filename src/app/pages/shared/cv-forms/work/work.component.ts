import { Output } from '@angular/core';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { CvFormService } from '../services/cv-form.service';
import { Work } from './work.object';

@Component({
  selector: 'work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss']
})
export class WorkComponent implements OnInit {

  work = new Work();
  @Output() onAddWork: EventEmitter<any> = new EventEmitter();
  workList: Work[] = [];
  isDone = false;

  constructor(private cvFormService: CvFormService) { }

  ngOnInit() {
    this.workList = this.cvFormService.getWork();
    for (const work of this.workList){
      this.work = work;
    }
  }

  submitWork() {
    this.workList.push(this.work);
  }
  done() {
    this.cvFormService.saveWork(this.workList);
    this.onAddWork.emit(this.workList);
    this.isDone = true;
  }

  resetWork() {
    this.work = new Work();
  }

}

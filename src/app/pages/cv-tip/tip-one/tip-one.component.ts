import { EventEmitter } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'tip-one',
  templateUrl: './tip-one.component.html',
  styleUrls: ['./tip-one.component.scss']
})
export class TipOneComponent implements OnInit {

  @Output() next: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  nextTip(){
   this.next.emit(true);
  }

}

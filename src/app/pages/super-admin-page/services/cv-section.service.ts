import { EventEmitter, Injectable } from '@angular/core';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { HttpRequestService } from '../../../services/http-request.service';
import { CvSection } from '../cv-sections/cv-section.object';

@Injectable({
  providedIn: 'root'
})
export class CvSectionService {

  token = this.authSerice.getUserToken();
  cvSectionListEmitter = new EventEmitter<CvSection>();
  responseEmitter = new EventEmitter();
  errorEmitter = new EventEmitter();

  constructor(private httprequest: HttpRequestService, private authSerice: CustomeAuthService) { }

  addCvSection(cvSection: CvSection) {
    this.httprequest.sendPostRequest('superadmin/section/add', cvSection, this.token)
      .subscribe(
        data => {
          console.log(data);
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          const messages = Object.values(error.error.error);
          this.errorEmitter.emit(messages);
        }
      );
  }
  // updateCvSection(cvSection: CvSection) {
  //   this.httprequest.sendPatchRequest('CvSection/update', cvSection, this.token)
  //     .subscribe(
  //       data => {
  //         console.log(data);
  //         this.responseEmitter.emit(data);
  //       },
  //       error => {
  //         console.log(error);
  //         const messages = Object.values(error.error.error);
  //         this.errorEmitter.emit(messages);
  //       }
  //     );
  // }
  deleteCvSection(id: string) {
    this.httprequest.sendDeleteRequest('superadmin/section/' + id, this.token)
      .subscribe(
        data => {
          console.log(data);
          this.responseEmitter.emit(data);
        },
        error => {
          const messages = Object.values(error.error.error);
          this.errorEmitter.emit(messages);
        }
      );
  }
  getAllCvSections() {
    this.httprequest.sendGetRequest('v1/common/sections', this.token).subscribe(
        data => {
          this.processGetCvSections(data);
        },
        error => {
          console.log('Something Went Wrong!!!', error);
        },
    );
  }
  processGetCvSections(data: any) {
    if (data) {
      this.cvSectionListEmitter.emit(data);
    }
  }
}

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountSettingComponent } from './account-setting/account-setting.component';
import { AdminGuard } from '../services/admin.guard';
import { SuperAdminGuard } from '../services/super-admin.guard';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { UserGuard } from '../services/user.guard';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'account-setting',
      component: AccountSettingComponent,
    },
    {
      path: 'unauthorized',
      component: UnauthorizedComponent,
    },
    {
      path: 'upload-cv',
      canActivate: [UserGuard],
      loadChildren: () => import('./upload-cv/upload-cv.module')
        .then(m => m.UploadCVModule),
    },
    {
      path: 'cv-history',
      canActivate: [UserGuard],
      loadChildren: () => import('./cv-history/cv-history.module')
        .then(m => m.CvHistoryModule),
    },
    {
      path: 'cv-tip',
      canActivate: [UserGuard],
      loadChildren: () => import('./cv-tip/cv-tip.module')
        .then(m => m.CvTipModule),
    },
    {
      path: 'super-admin-page',
      canActivate: [SuperAdminGuard],
      loadChildren: () => import('./super-admin-page/super-admin-page.module')
        .then(m => m.SuperAdminPageModule),
    },
    {
      path: 'admin-page',
      canActivate: [AdminGuard],
      loadChildren: () => import('./admin-page/admin-page.module')
        .then(m => m.AdminPageModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}

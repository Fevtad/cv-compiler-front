export class Work {
    public job_title: string;
    public employer: string;
    public description: string;
    public from: string;
    public to: string;
    constructor() {
        this.job_title = null;
        this.employer = null;
        this.description = null;
        this.from = null;
        this.to = null;
    }
}
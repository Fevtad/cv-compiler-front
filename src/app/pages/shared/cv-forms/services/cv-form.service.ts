import { EventEmitter, Injectable } from '@angular/core';
import { Education } from '../education/education.object';
import { Personal } from '../personal/personal.object';
import { Reference } from '../reference/reference.object';
import { Skill } from '../skill/skill.object';
import { Work } from '../work/work.object';

@Injectable({
  providedIn: 'root'
})
export class CvFormService {

  personal = new Personal();
  skill = new Skill();
  educationList: Education[] = [];
  achievement = [];
  workList: Work[] = [];
  referenceList: Reference[] = [];

  constructor() { }

  savePersonal(personal) {
    this.personal = personal;
  }
  getPersonal() {
    return this.personal;
  }
  saveSkill(skill) {
    this.skill = skill;
  }
  getSkill() {
    return this.skill;
  }
  saveWork(work) {
    this.workList = work;
  }
  getWork() {
    return this.workList;
  }
  saveReference(reference) {
    this.referenceList = reference;
  }
  getReference() {
    return this.referenceList;
  }
  saveAchievement(achievement) {
    this.achievement = achievement;
  }
  getAchievement() {
    return this.achievement;
  }
  saveEducation(education) {
    this.educationList = education;
  }
  getEducation() {
    return this.educationList;
  }
}

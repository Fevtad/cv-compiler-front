import { EventEmitter, Injectable } from '@angular/core';
import { User } from '../../../@core/data/users';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { HttpRequestService } from '../../../services/http-request.service';
import { UserCV } from '../objects/user-cv.object';

@Injectable({
  providedIn: 'root'
})
export class RecommendationService {
  token = this.authSerice.getUserToken();
  responseEmitter = new EventEmitter();
  errorEmitter = new EventEmitter();

  constructor(private httprequest: HttpRequestService, private authSerice: CustomeAuthService) { }

  addRecommendation(cvId: string, recommendations: any) {
    this.httprequest.sendPatchRequest('v1/admin/recommendation/' + cvId , {recommendations} , this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.msg);
        }
      );
  }
  sendEmail(userId) {
    this.httprequest.sendPostRequest('v1/admin/emailrecommendation', {userId} , this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.msg);
        }
      );
  }
}

import { Component, OnInit } from '@angular/core';
import { CustomeAuthService } from '../../custome-auth/custome-auth.service';
import { UsersService } from '../super-admin-page/services/user.service';
import { User } from '../super-admin-page/users/user.object';

@Component({
  selector: 'account-setting',
  templateUrl: './account-setting.component.html',
  styleUrls: ['./account-setting.component.css']
})
export class AccountSettingComponent implements OnInit {

  selectedUser = new User();
  errorMessage = [];
  successMessage = null;

  constructor(private authService: CustomeAuthService, private userService: UsersService) { }

  ngOnInit() {
  }
  updateAccountSettingComponent() {
    if (this.selectedUser.firstName) {
      this.authService.setFirstName(this.selectedUser.firstName);
    } if (this.selectedUser.lastName) {
      this.authService.setLastName(this.selectedUser.lastName);
    }
    setTimeout(() => {
      this.errorMessage = [];
      this.successMessage = null;
    }, 5000);
  }

  isUser() {
    return this.authService.isUser();
  }
  isSuperAdmin() {
    return this.authService.isSuperAdmin();
  }
  updateUser() {
    console.log(this.selectedUser);
    // Object.keys(this.selectedUser).forEach((key) => (this.selectedUser[key] == null) && delete this.selectedUser[key]);
    // this.userService.updateUser(this.selectedUser);
    // this.userService.responseEmitter.subscribe(data => {
    //   this.updateAccountSettingComponent();
    //   this.successMessage = data.message;
    // });

    // this.userService.errorEmitter.subscribe(
    //   error => {
    //     this.errorMessage = error.slice();
    //   }
    // );
  }
}

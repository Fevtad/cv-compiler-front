import { Reference } from '@angular/compiler/src/render3/r3_ast';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomeAuthService } from '../../../../custome-auth/custome-auth.service';

@Component({
  selector: 'reference-view',
  templateUrl: './reference-view.component.html',
  styleUrls: ['./reference-view.component.scss']
})
export class ReferenceViewComponent implements OnInit {

  @Input() referenceSection;
  @Input() cvId: string;
  @Output() saveReferenceVersion: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateRecommendation: EventEmitter<any> = new EventEmitter();
  @Output() onUpdateCv: EventEmitter<any> = new EventEmitter();
  selectedReferenceList: Reference[] = [];
  successMessage = null;
  errorMessage = [];
  type = 'view';

  constructor(private authService: CustomeAuthService) { }

  ngOnInit() {
    this.saveReferenceVersion.emit(this.referenceSection);
  }
  selectList(list: Reference[]) {
    this.selectedReferenceList = list;
    this.type = 'edit';
  }

  // when save is clicked
  saveReferenceList(ReferenceList: Reference[]) {
    this.referenceSection.description = ReferenceList;
    if (this.isUser()) {
      // update cv for user
      this.onUpdateCv.emit(this.referenceSection);
      // emit to the parent for version saving
      this.saveReferenceVersion.emit(this.referenceSection);
    } else if (this.isAdmin()){
       this.onUpdateRecommendation.emit(this.referenceSection);
    }
    this.type = 'view';
  }
  keep(){
    this.saveReferenceList(this.referenceSection.uploaded);
  }
  replace(){
    this.saveReferenceList(this.referenceSection.recommended);
  }
  isUser() {
    return this.authService.isUser();
  }
  isAdmin() {
    return this.authService.isAdmin();
  }
}

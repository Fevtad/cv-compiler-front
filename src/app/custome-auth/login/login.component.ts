import { Component, OnInit } from '@angular/core';
import { NewUser } from '../new-user.object';
import { CustomeAuthService } from '../custome-auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  showMessages: any = '';
  strategy: string = '';
  errorMessage = null;
  user = new NewUser();
  submitted: boolean = false;

  constructor(private authService: CustomeAuthService, private router: Router) { }

  ngOnInit() {
  }
  login(): void {
    this.authService.authenticate(this.user);
    this.authService.authSuccessEmitter.subscribe(
      data => {
        if (this.authService.isUser()){
          this.router.navigate(['/pages/dashboard']);
        } if (this.authService.isSuperAdmin()){
          this.router.navigate(['/pages/super-admin-page']);
        } if (this.authService.isAdmin()){
          this.router.navigate(['/pages/admin-page/all-cv']);
        }
      }
    );
    this.authService.authErrorEmitter.subscribe(
      error => {
        this.errorMessage = error;
        console.log(this.errorMessage);
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'photo-tip',
  templateUrl: './photo-tip.component.html',
  styleUrls: ['./photo-tip.component.scss']
})
export class PhotoTipComponent implements OnInit {

  photoOne = true;
  photoTwo = false;
  photoThree = false;

  constructor() { }

  ngOnInit() {
  }

}

import { TestBed } from '@angular/core/testing';

import { UserCVService } from './user-cv.service';

describe('UserCVService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserCVService = TestBed.get(UserCVService);
    expect(service).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentCvComponent } from './sent-cv.component';

describe('SentCvComponent', () => {
  let component: SentCvComponent;
  let fixture: ComponentFixture<SentCvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentCvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentCvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class CvSectionPaginator {
    public current_page: number;
    public first_page_url: string;
    public from: number;
    public last_page: number;
    public last_page_url: string;
    public next_page_url: string;
    public path: string;
    public per_page: number;
    public prev_page_url: string;
    public to: number;
    public total: number;
    public data: CvSection[];

    constructor() {
        this.current_page = null;
        this.from = null;
        this.last_page = null;
        this.per_page = null;
        this.to = null;
        this.total = null;
        this.first_page_url = '';
        this.last_page_url = '';
        this.next_page_url = '';
        this.prev_page_url = '';
        this.path = '';

    }
}
export class CvSection {
    public _id: string;
    public name: string;
    public category: string;
    constructor() {
        this._id = null;
        this.name = null;
        this.category = null;
    }
}
export class FormSection {
    public description: any;
    public name: string;
    public sectionId: string;
    constructor() {
        this.description = null;
        this.name = null;
        this.sectionId = null;
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuperAdminPageRoutingModule } from './super-admin-page-routing.module';
import { UsersComponent } from './users/users.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { CvSectionsComponent } from './cv-sections/cv-sections.component';
import { SuperAdminPageComponent } from './super-admin-page.component';
import { FormsModule } from '@angular/forms';
import { NbAlertModule, NbCardModule, NbButtonModule, NbInputModule } from '@nebular/theme';
import { Ng2TelInputModule } from 'ng2-tel-input';

@NgModule({
  declarations: [UsersComponent, AddUserComponent, CvSectionsComponent, SuperAdminPageComponent],
  imports: [
    CommonModule,
    NbAlertModule,
    FormsModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    Ng2TelInputModule,
    SuperAdminPageRoutingModule,
  ]
})
export class SuperAdminPageModule { }

import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'photo-tip-two',
  templateUrl: './photo-tip-two.component.html',
  styleUrls: ['./photo-tip-two.component.scss']
})
export class PhotoTipTwoComponent implements OnInit {

  @Output() next: EventEmitter<any> = new EventEmitter();
  @Output() previous: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  nextPhoto(){
    this.next.emit(true);
  }
  previousPhoto(){
    this.previous.emit(true);
  }

}

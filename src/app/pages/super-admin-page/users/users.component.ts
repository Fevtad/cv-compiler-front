import { Component, OnInit, TemplateRef } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { UsersService } from '../services/user.service';
import { User, UserPaginator } from './user.object';

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  newUser = new User();
  selectedUser = new User();
  paginatedUserList = new UserPaginator();
  successMessage = null;
  errorMessage = [];
  userList: User[] = [];

  constructor(private userService: UsersService, private dialogService: NbDialogService) { }

  ngOnInit() {
    this.updateUsersComponent();
    this.userService.userListEmitter.subscribe(
      data => {
        this.userList = data;
      },
    );
  }
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      { context: 'You\'re done!' });
  }
  updateUsersComponent() {
    this.userService.getAllPaginatedUsers();
    setTimeout(() => {
      this.errorMessage = [];
      this.successMessage = null;
    }, 5000);
  }
  onNewUserAdded(newUser) {
    this.userService.addUser(newUser);
    this.userService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateUsersComponent();
    });

    this.userService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error.slice();
      }
    );
  }

  selectUser(user: User) {
    this.selectedUser = user;
  }

  deleteUser() {
    this.userService.deleteUser(this.selectedUser._id);
    this.userService.responseEmitter.subscribe(data => {
      this.successMessage = data.msg;
      this.updateUsersComponent();
    });
    this.userService.errorEmitter.subscribe(
      error => {
        this.errorMessage = error.slice();
      },
    );
  }

}

export class Skill {
    public language: Language[];
    public soft_skill: any;
    public hard_skill: any;
    constructor() {
        this.language = [];
        this.soft_skill = [];
        this.hard_skill = [];
    }
}

export class Language {
    public type: string;
    public level: string;
    constructor() {
        this.type = null;
        this.level = null;
    }
}
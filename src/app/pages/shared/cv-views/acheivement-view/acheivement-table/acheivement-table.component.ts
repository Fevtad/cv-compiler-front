import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'acheivement-table',
  templateUrl: './acheivement-table.component.html',
  styleUrls: ['./acheivement-table.component.scss']
})
export class AcheivementTableComponent implements OnInit {

  @Input() acheivementList: any;
  @Input() type: string;
  @Output() onCancel: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  cancel(){
    this.onCancel.emit(true);
  }
  save(){
    this.onSave.emit(this.acheivementList);
  }
  onChange(event, index){
    this.acheivementList[index] = event.target.value;
  }


}

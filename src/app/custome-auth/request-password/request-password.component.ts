import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { CustomeAuthService } from '../custome-auth.service';
import { NewUser } from '../new-user.object';

@Component({
  selector: 'request-password',
  templateUrl: './request-password.component.html',
  styleUrls: ['./request-password.component.scss']
})
export class RequestPasswordComponent {

  user = new NewUser();
  successMessage = null;
  errorMessage = null;

  constructor(private authService: CustomeAuthService) { }

  requestPass() {
    this.authService.requestPassword(this.user.email);
    this.authService.authSuccessEmitter.subscribe(
        data => {
              this.successMessage = data.msg;
        }
    );
    this.authService.authErrorEmitter.subscribe(
        error => {
          this.errorMessage = error;
        }
    );
  }

}

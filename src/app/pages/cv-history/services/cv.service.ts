import { EventEmitter, Injectable } from '@angular/core';
import { CustomeAuthService } from '../../../custome-auth/custome-auth.service';
import { HttpRequestService } from '../../../services/http-request.service';
import { UserCV } from '../../admin-page/objects/user-cv.object';

@Injectable({
  providedIn: 'root'
})
export class CvService {

  token = this.authSerice.getUserToken();
  userCVListEmitter = new EventEmitter<UserCV>();
  historyListEmitter = new EventEmitter();
  historyEmitter = new EventEmitter();
  responseEmitter = new EventEmitter();
  errorEmitter = new EventEmitter();

  constructor(private httprequest: HttpRequestService, private authSerice: CustomeAuthService) { }

  getUserCvDetail(id: string) {
    this.httprequest.sendGetRequest('v1/user/cv/' + id, this.token).subscribe(
      data => {
        this.processGetUserCvDetail(data);
      },
      error => {
        console.log('Something Went Wrong!!!', error);
      },
    );
  }
  processGetUserCvDetail(data: any) {
    if (data) {
      this.userCVListEmitter.emit(data);
    }
  }
  updateCv(cvId: string, sections: any) {
    this.httprequest.sendPatchRequest('v1/user/cv/' + cvId, {sections}, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.msg);
        }
      );
  }
  addCvHistory(cv: string, sections: any) {
    console.log(cv, sections);
    this.httprequest.sendPostRequest('v1/user/cvhistory', {cv, sections}, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.msg);
        }
      );
  }
  deleteCvHistory(id: string) {
    this.httprequest.sendDeleteRequest('v1/user/cvhistory/' + id, this.token)
      .subscribe(
        data => {
          this.responseEmitter.emit(data);
        },
        error => {
          console.log(error);
          this.errorEmitter.emit(error.error.msg);
        }
      );
  }
  getCvHistory(id: string) {
    this.httprequest.sendGetRequest('v1/user/cvhistory/' + id, this.token).subscribe(
      data => {
        this.processGetCvHistory(data);
      },
      error => {
        console.log('Something Went Wrong!!!', error);
      },
    );
  }
  processGetCvHistory(data: any) {
    console.log(data)
    if (data) {
      this.userCVListEmitter.emit(data);
    }
  }
  getAllHistory(userId: string) {
    this.httprequest.sendGetRequest('v1/user/cvhistorys/' + userId, this.token).subscribe(
      data => {
        this.processGetAllHistory(data);
      },
      error => {
        console.log('Something Went Wrong!!!', error);
      },
    );
  }
  processGetAllHistory(data: any) {
    if (data) {
      this.historyListEmitter.emit(data);
    }
  }
}

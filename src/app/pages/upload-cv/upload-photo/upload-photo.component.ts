import { Component, OnInit, TemplateRef } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { UploadCvService } from '../upload-cv.service';

@Component({
  selector: 'upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.css']
})
export class UploadPhotoComponent implements OnInit {

  errorMessage = null;
  successMessage = null;
  changePhoto = false;

  constructor(private dialogService: NbDialogService, private uploadCvService: UploadCvService) { }

  ngOnInit() {
  }
  open(dialog: TemplateRef<any>) {
    this.dialogService.open(
      dialog,
      { context: 'You\'re done!' });
  }
  updateUploadPhotoComponent() {
    setTimeout(() => {
      this.errorMessage = null;
      this.successMessage = null;
    }, 5000);
  }
  handleFileInput(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadCvService.uploadImage(file);
      this.uploadCvService.responseEmitter.subscribe(
        data => {
          this.successMessage = 'You are done! Our team will get back to you soon';
          this.changePhoto = true;
          this.updateUploadPhotoComponent();
        }
      );
      this.uploadCvService.errorEmitter.subscribe(
        error => {
          this.errorMessage = error;
          this.updateUploadPhotoComponent();
        }
      );
    }
  }
}

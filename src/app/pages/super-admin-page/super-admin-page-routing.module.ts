import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuperAdminPageComponent } from './super-admin-page.component';

const routes: Routes = [
  {
    path: '',
    component: SuperAdminPageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperAdminPageRoutingModule { }

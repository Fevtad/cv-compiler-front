import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomeAuthService } from '../custome-auth.service';

@Component({
  selector: 'verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

  constructor(private authService: CustomeAuthService, private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      param => {
        this.authService.verify(param.token);
        this.authService.authSuccessEmitter.subscribe(
          data => {
              this.router.navigate(['/auth/login']);
          }
        );
        this.authService.authErrorEmitter.subscribe(
          data => {
              this.router.navigate(['/not-found']);
          }
        );
      }
    );
  }

}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaginatorService } from '../services/paginator.service';

@Component({
  selector: 'paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

    @Output() onLoadingData = new EventEmitter();
    @Output() onDataLoaded = new EventEmitter();
    @Input() paginatedData: any;
    public paginateSize = 10;
    public paginateSizeOptions = [2, 5, 10, 50, 100, 500, 1000];
    public paginatePageNum = 1;
    public paginateSizePos = 1;

    constructor(private paginatorService: PaginatorService) {
    }

    ngOnInit() {
        this.paginatorService.PaginatedObjectListEmitter.subscribe(
            data => {
                this.paginatedData = data;
                this.onDataLoaded.emit(data);
            }
        );
    }

    public changePaginateSize() {
        console.log(this.paginateSize);
        this.onLoadingData.emit(true);
        this.paginatorService.getPaginatedData(this.paginatedData.path + '?page=' + this.paginatePageNum
            + '&PAGINATE_SIZE=' + this.paginateSize);
    }

    public goToNextPage(direction: number) {
        if (direction === -1) {
            if (this.paginatedData.current_page <= 1) {
                return true;
            }
        } else {
            if (this.paginatedData.current_page >= this.paginatedData.last_page) {
                return true;
            }
        }
        this.onLoadingData.emit(true);
        this.paginatePageNum = this.paginatedData.current_page + direction;
        this.paginatorService.getPaginatedData(this.paginatedData.path + '?page=' + this.paginatePageNum
            + '&PAGINATE_SIZE=' + this.paginateSize);
    }

    public goToLastPages(lastPage: boolean) {
        this.onLoadingData.emit(true);
        if (lastPage) {
            console.log(this.paginateSize);
            this.paginatorService.getPaginatedData(this.paginatedData.last_page_url + '&PAGINATE_SIZE=' + this.paginateSize);
        } else {
            this.paginatorService.getPaginatedData(this.paginatedData.first_page_url + '&PAGINATE_SIZE=' + this.paginateSize);
        }
    }

}

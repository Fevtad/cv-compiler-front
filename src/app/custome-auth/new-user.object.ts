export class NewUser{
    public email: string;
    public password: string;
    public confirmPassword: string;
    public firstName: string;
    public lastName: string;
    public phoneNumber: string;
    public role: string;
    constructor(){
        this.email = null;
        this.password = null;
        this.firstName = null;
        this.lastName = null;
        this.phoneNumber = null;
        this.role = null;
    }
}
